package com.liceolapaz.xestordeimaxes;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class VisorController {

    public TextField directorio;
    public Label filename;
    public ImageView imageView;
    Stream<Path> listfiles;
    List<Path> listImages;

    int currentImage;

    public void onChangeDirectory(ActionEvent actionEvent) {
        Path newDir = Paths.get(directorio.getText().trim());
        // comprobar o campo de texto
         if (Files.isDirectory(newDir)){
             try {
                 //Stream<Path> listfiles = Files.list(newDir);
                 listfiles = Files.list(newDir).filter(path -> path.getFileName().toString().endsWith(
                         ".jpg"));
                 currentImage = 0;
                 listImages = new ArrayList<>();
                 listfiles.forEach(path -> {
                     System.out.println(path.getFileName());
                     listImages.add(path);
                         }
                 );

                 System.out.println(listfiles);
             } catch (IOException e) {
                 //throw new RuntimeException(e);
                 System.out.println(e.getMessage());
             }
         }
        // intentar abrir o directorio que teña ese path
        // lista ficheiros (solo imaxes) e gardala
        // ensinar unha desas imaxes e o seu nome de ficheiro
        try {
            updateImage();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    private void updateImage() throws IOException {
        Path currentPath =  listImages.get(currentImage);
        String stringPath = currentPath.toString();

        //InputStream is = Files.newInputStream(currentPath);
        //Image imageActual = new Image(is);

        Image imageActual = new Image("file:" +stringPath);

        imageView.setImage(imageActual);
        filename.setText(currentPath.getFileName().toString());

        
    }

    public void onPrevImg(ActionEvent actionEvent) {
        // ver a imaxe anterior da lista
        if (--currentImage <0){
            currentImage = listImages.size()-1;
        }
        try {
            updateImage();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        // ollo cando chegamos á posición 0
    }

    public void onNextImg(ActionEvent actionEvent) {
        // ver a imaxe seguinte da lista
        // ollo cando chegamos ao final da lista
        // ver a imaxe anterior da lista
        if (currentImage++ > listImages.size()){
            currentImage = 0;
        }
        try {
            updateImage();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}