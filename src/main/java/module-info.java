module com.liceolapaz.xestordeimaxes {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.liceolapaz.xestordeimaxes to javafx.fxml;
    exports com.liceolapaz.xestordeimaxes;
}